# TITANIC PREDICTOR 3000

## Identifying your business goals :

Following the tragic events of the Titanic that took place on the night of April 14-15, 1912, many questions arose.
Indeed, it appeared that not all passengers had the same chance of survival.
This is due to many criteria such as luck but also to more important criteria such as gender or passenger class.

Therefore, thanks to the tools of Machine Learning, we will be able to say who could have survived this disaster or not.
All this in order to possibly adapt it to larger boats and improve maritime safety.

## Assessing your situation : 

The data we have are divided into 2 files.
One contains the training data, where the results are listed, i.e. whether or not passengers survive that depend on the criteria.
The other is the test data on which we will apply our model.
(Access to the data is perfectly legal.

There are no real causes that could delay our work, only if our computers fail. To compensate for this, we will record our work on online sites each time, so it will always be very easy for us to pick up where we left off.

The terminology for our project is quite basic, if we realize that there are specific terms, they will be explained in due course.

Our project is not business, it has no cost or benefit, doc there is no problem for the study of this project.

## Data-mining goals

As we have said, this project is not business, so it does not necessarily have a specific purpose. 
However, it is possible to have access to some data that will prove that this method works well.

Indeed, the aim here is to achieve the highest possible level of accuracy, i.e. to be able to accurately determine which passengers survive or not.

So, if one day we are lucky enough to be asked to work on larger boats, we could perhaps find loopholes in their maritime surveillance system and avoid these kinds of accidents.
